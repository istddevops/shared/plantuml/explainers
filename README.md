# PlantUML Explainers

![PlantUML](https://gitlab.com/uploads/-/system/group/avatar/61964325/plantuml.png?width=64)
![Explainers](https://gitlab.com/uploads/-/system/project/avatar/42207101/explainer.png?width=64)

PlantUML componenten om diagrammen samen te stellen voor uitleg van zaken rondom de gegevensuitwisseling in zorg.

- [PlantUML Sprites](sprites.md)
- [PlantUML Procedures](procedures.md)
