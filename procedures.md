# PlantUML Procedures

Maatwerk procedures die momenteel in [PlantUML Explainers Lib](Explainers_Lib.puml) beschikbaar zijn:


## Uitgever

```txt
Uitgever($naam="Uitgever", $color="red", $scale=1)
```

```plantuml
@startuml
!include https://gitlab.com/istddevops/shared/plantuml/explainers/-/raw/main/Explainers_Lib.puml

Uitgever()

@enduml
```
