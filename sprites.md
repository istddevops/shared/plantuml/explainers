# PlantUML Sprites

Dit is de lijst met Sprites die momenteel in de [PlantUML Explainers Lib](Explainers_Lib.puml) worden gebruikt:

```plantuml
@startuml

!include https://gitlab.com/istddevops/shared/plantuml/explainers/-/raw/main/Explainers_Lib.puml

listsprites

@enduml
```
